package org.apache.dubbo.rpc.cluster.loadbalance.adaptive;

import org.apache.dubbo.rpc.Invoker;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

public class Node<T> {
    //默认latency值(250s)
    private static final long DEAFULT_LATENCY = 250_000_000_000L;
    //惩罚值
    private static final int PENALTY = 10;
    //衰减系数(600ms)
    private static final long TAU = 600_000_000L;
    private static final Supplier<Long> clock = System::nanoTime;
    private String host;
    //默认权重值
    private final AtomicInteger weight = new AtomicInteger(1);

    private final AtomicInteger inflight = new AtomicInteger(0);
    private final AtomicInteger success = new AtomicInteger(1000);
    private final AtomicLong latency = new AtomicLong();
    private Invoker<T> invoker;

    public Node(String host,Invoker<T> invoker){
        this.host = host;
        this.invoker = invoker;
    }

    public Node (Invoker<T> invoker){
        this.host = invoker.getUrl().getBackupAddress();
        this.invoker = invoker;
    }

    public Node(String host,int weight,Invoker<T> invoker){
        this.host=host;
        this.invoker = invoker;
        this.weight.set(weight);
    }

    public Invoker<T> getInvoker(){
        return this.invoker;
    }

    public boolean health(){
        return getSuccess()>500;
    }

    public int getWeight() {
        return this.weight.get();
    }

    public int getSuccess(){
        return this.success.get();
    }

    public AtomicInteger getInflight(){
        return this.inflight;
    }

    public long load(){
        long latency = (long)(Math.sqrt(this.latency.get())+1);
        long load = latency * this.inflight.get();
        if (load==0){
            return DEAFULT_LATENCY;
        }
        return load;
    }

    public void updateState(long startTime,int weight,boolean error){
        this.inflight.decrementAndGet();
        long now = clock.get();
        long cost = now - startTime;
        //事实计算β，公式 β = e^(-t/k)
        double beta  =  Math.exp((double)-cost/(double)TAU);
        long latency = now - startTime;
        if (latency < 0 ){
            latency = 0;
        }
        long oldlatency = this.latency.get();
        int success = error? 0 : 1000;
        //降权操作
        int oldWeight = this.weight.get();
        oldWeight = error?oldWeight-10:oldWeight;
        if (oldWeight<1){
            oldWeight = 1;
        }
        weight = oldWeight;
        int oldSuccess = this.success.get();
        //移动加权平均
        latency = (int)((double)(oldlatency*beta)+(double)(latency*(1-beta)));
        success = (int)((double)(oldSuccess*beta)+(double)(success*(1-beta)));
        this.latency.set(latency);
        this.latency.set(success);
        this.weight.set(weight);
    }
}
