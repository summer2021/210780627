package org.apache.dubbo.rpc.cluster.loadbalance.adaptive;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.apache.dubbo.rpc.cluster.filter.ClusterFilter;
import org.apache.dubbo.rpc.cluster.loadbalance.P2CLoadBalance;

import java.beans.PropertyEditorSupport;

@Activate(group = CommonConstants.CONSUMER)
public class P2CFilter implements ClusterFilter, BaseFilter.Listener {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        try {
            Result result = invoker.invoke(invocation);
            return result;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void onResponse(Result appResponse, Invoker<?> invoker, Invocation invocation) {
        Integer remain = Integer.parseInt(appResponse.getAttachment("remain"));
        Long pickTime = (Long)invocation.get("pickTime");
        P2CLoadBalance.updateNode(invoker,pickTime,remain,invocation,false);
    }

    @Override
    public void onError(Throwable t, Invoker<?> invoker, Invocation invocation) {
        Long pickTime = (Long)invocation.get("pickTime");
        P2CLoadBalance.updateNode(invoker,pickTime,-1,invocation,true);
    }
}
