package org.apache.dubbo.rpc.cluster.loadbalance;

import org.apache.dubbo.common.URL;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.cluster.loadbalance.adaptive.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;

public class P2CLoadBalance extends AbstractLoadBalance{

    //闲置的最大时间(3s)
    private static final long IDLE_GAP = 3000_000_000L;
    private static final Supplier<Long> CLOCK = System::nanoTime;

    private static final ConcurrentHashMap<String,List<Node>> method2Nodes = new ConcurrentHashMap<>();

    @Override
    protected <T> Invoker<T> doSelect(List<Invoker<T>> invokers, URL url, Invocation invocation) {
        //update method2invokers
        updateMethod2Invokers(invocation.getMethodName(),invokers);
        //save pickTime
        long pickTime = CLOCK.get();
        invocation.put("pickTime",pickTime);
        //prepick
        Node[] randomNodes = prepick(invocation,invokers);
        //pick
        Node pick = pick(randomNodes);
        return pick.getInvoker();
    }

    /**
     *        nodeA.load                           nodeB.load
     * ----------------------------   :   ----------------------------
     * nodeA.success * nodeA.weight        nodeB.success * nodeB.weight
    */
    private <T> Node pick(Node[] randomNodes){
        Node nodeA = randomNodes[0];
        Node nodeB = randomNodes[1];
        //默认反选
        Node pc = nodeA;
        Node upc= nodeB;
        if (nodeA.load()*nodeB.getSuccess()*nodeB.getWeight()<=nodeB.load()*nodeA.getSuccess()*nodeA.getWeight()){
            pc = nodeA;
            upc = nodeB;
        }
        pc.getInflight().incrementAndGet();
        return pc;
    }

    private <T> Node[] prepick(Invocation invocation,List<Invoker<T>> invokers){
        Node[] randomNodes = new Node[2];
        List<Node> nodes = method2Nodes.get(invocation.getMethodName());
        for (int i = 0; i < 3; i++) {
            int first = ThreadLocalRandom.current().nextInt(nodes.size());
            int second = ThreadLocalRandom.current().nextInt(nodes.size()-1);
            randomNodes[0] = nodes.get(first);
            randomNodes[1] = nodes.get(second);
            if (invokers.contains(randomNodes[0].getInvoker())||invokers.contains(randomNodes[1].getInvoker())){
                break;
            }
        }
        return randomNodes;
    }

    private final <T> void updateMethod2Invokers(String methodName,List<Invoker<T>> invokers){
        method2Nodes.computeIfAbsent(methodName,k-> new ArrayList<Node>());
        List<Node> nodes = method2Nodes.get(methodName);
        invokers.forEach(ivk->{
            boolean exist = false;
            for (Node node : nodes) {
                if (node.getInvoker()==ivk){
                    exist =  true;
                    break;
                }
            }
            if (!exist){
                nodes.add(new Node(ivk));
            }
        });
    }

    public static void updateNode(Invoker invoker,Long pickTime,int remain,Invocation invocation,boolean error){
        String method = invocation.getMethodName();
        List<Node> nodes = method2Nodes.get(method);
        for (Node node : nodes) {
            if (node.getInvoker().equals(invoker)){
                node.updateState(pickTime,remain,error);
                break;
            }
        }
    }
}
