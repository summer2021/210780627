package org.apache.dubbo.rpc.filter;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.apache.dubbo.rpc.limiter.AbstractLimiter;
import org.apache.dubbo.rpc.limiter.Limiter;
import org.apache.dubbo.rpc.limiter.SimpleLimiter;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

@Activate(group = CommonConstants.PROVIDER)
public class PartionedFilter implements Filter,BaseFilter.Listener {

    private ConcurrentHashMap<String,Limiter> partions;
    private Limiter limiter;


    public static class Builder {
        private Limiter limiter = SimpleLimiter.newDeafult();
        private ConcurrentHashMap<String,Limiter> partions = new ConcurrentHashMap<>();

        public Builder setLimiter(Limiter limiter){
            this.limiter = limiter;
            return this;
        }

        public Builder partions(List<String> names){
            names.stream()
                .forEach(new Consumer<String>() {
                    @Override
                    public void accept(String s) {
                        partions.computeIfAbsent(s,(str)->{
                            return SimpleLimiter.newDeafult();
                        });
                    }
                });
            return this;
        }

        public PartionedFilter build(){
            return new PartionedFilter(this);
        }
    }

    public PartionedFilter(Builder builder){
        this.partions = builder.partions;
        this.limiter = builder.limiter;
    }

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        //do partion
        partions.computeIfAbsent(invocation.getMethodName(), (s) -> {
            try {
                //TODO 不同的limiter
                return SimpleLimiter.newDeafult();
            } catch (Exception e) {
                e.printStackTrace();
                //throw new Exception
                return null;
            }
        });
        Limiter limiter = partions.get(invocation.getMethodName());
        //acquire
        Optional<Limiter.Listener> Listener = limiter.acquire();
        if (Listener.isEmpty()){
            throw new RpcException(RpcException.LIMIT_EXCEEDED_EXCEPTION,
                "Waiting concurrent invoke timeout in client-side for service:  " +
                    invoker.getInterface().getName() + ", method: " + invocation.getMethodName() +
                    ", limit: " + "" );
        }
        //add listener
        invocation.put("AdaptiveListener",Listener);
        return invoker.invoke(invocation);
    }

    @Override
    public void onResponse(Result appResponse, Invoker<?> invoker, Invocation invocation) {
        Optional<Limiter.Listener> listener = (Optional<Limiter.Listener>) invocation.get("AdaptiveListener");
        listener.get().onSuccess();
        AbstractLimiter limiter = (AbstractLimiter)partions.get(invocation.getMethodName());
        appResponse.setAttachment("remain",limiter.getRemain());
    }

    @Override
    public void onError(Throwable t, Invoker<?> invoker, Invocation invocation) {
        Optional<Limiter.Listener> listener = (Optional<Limiter.Listener>) invocation.get("AdaptiveListener");
        listener.get().onDropped();
    }
}
