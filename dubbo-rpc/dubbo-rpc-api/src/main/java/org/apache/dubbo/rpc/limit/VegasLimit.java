package org.apache.dubbo.rpc.limit;

import org.apache.dubbo.rpc.limit.function.Log10RootFunction;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class VegasLimit extends AbstractLimit {

    private static final Function<Integer, Integer> LOG10 = Log10RootFunction.create(0);


    public static class Builder {
        private int initialLimit = 20;
        private int maxConcurrency = 1000;
        private double smoothing = 1.0;

        private Function<Integer, Integer> alphaFunc = (limit) -> 3 * LOG10.apply(limit.intValue());
        private Function<Integer, Integer> betaFunc = (limit) -> 6 * LOG10.apply(limit.intValue());
        private Function<Integer, Integer> thresholdFunc = (limit) -> LOG10.apply(limit.intValue());
        private Function<Double, Double> increaseFunc = (limit) -> limit + LOG10.apply(limit.intValue());
        private Function<Double, Double> decreaseFunc = (limit) -> limit - LOG10.apply(limit.intValue());
        private int probeMultiplier = 30;

        private Builder() {
        }

        public Builder probeMultiplier(int probeMultiplier) {
            this.probeMultiplier = probeMultiplier;
            return this;
        }

        public Builder alpha(int alpha) {
            this.alphaFunc = (ignore) -> alpha;
            return this;
        }

        public Builder threshold(Function<Integer, Integer> threshold) {
            this.thresholdFunc = threshold;
            return this;
        }

        public Builder alpha(Function<Integer, Integer> alpha) {
            this.alphaFunc = alpha;
            return this;
        }

        public Builder beta(int beta) {
            this.betaFunc = (ignore) -> beta;
            return this;
        }

        public Builder beta(Function<Integer, Integer> beta) {
            this.betaFunc = beta;
            return this;
        }

        public Builder increase(Function<Double, Double> increase) {
            this.increaseFunc = increase;
            return this;
        }

        public Builder decrease(Function<Double, Double> decrease) {
            this.decreaseFunc = decrease;
            return this;
        }

        public Builder smoothing(double smoothing) {
            this.smoothing = smoothing;
            return this;
        }

        public Builder initialLimit(int initialLimit) {
            this.initialLimit = initialLimit;
            return this;
        }

        @Deprecated
        public Builder tolerance(double tolerance) {
            return this;
        }

        public Builder maxConcurrency(int maxConcurrency) {
            this.maxConcurrency = maxConcurrency;
            return this;
        }

        @Deprecated
        public Builder backoffRatio(double ratio) {
            return this;
        }


        public VegasLimit build() {
            return new VegasLimit(this);
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static VegasLimit newDefault() {
        return newBuilder().build();
    }


    private volatile double estimatedLimit;

    private volatile long rtt_noload = 0;


    private final int maxLimit;

    private final double smoothing;
    private final Function<Integer, Integer> alphaFunc;
    private final Function<Integer, Integer> betaFunc;
    private final Function<Integer, Integer> thresholdFunc;
    private final Function<Double, Double> increaseFunc;
    private final Function<Double, Double> decreaseFunc;
    private final int probeMultiplier;
    private int probeCount = 0;
    private double probeJitter;

    private VegasLimit(Builder builder) {
        super(builder.initialLimit);
        this.estimatedLimit = builder.initialLimit;
        this.maxLimit = builder.maxConcurrency;
        this.alphaFunc = builder.alphaFunc;
        this.betaFunc = builder.betaFunc;
        this.increaseFunc = builder.increaseFunc;
        this.decreaseFunc = builder.decreaseFunc;
        this.thresholdFunc = builder.thresholdFunc;
        this.smoothing = builder.smoothing;
        this.probeMultiplier = builder.probeMultiplier;

        resetProbeJitter();

    }

    private void resetProbeJitter() {
        probeJitter = ThreadLocalRandom.current().nextDouble(0.5, 1);
    }

    private boolean shouldProbe() {
        return probeJitter * probeMultiplier * estimatedLimit <= probeCount;
    }

    @Override
    protected int _update(long startTime, long rtt, int inflight, boolean didDrop) {
        if (rtt<=0){
            throw new IllegalArgumentException("rtt must >= 0 ");
        }

        probeCount++;
        //阶段性探测，并不是每次请求都需要更新estimatedLimit的
        if (shouldProbe()) {
            resetProbeJitter();
            //探测一次以后把probecount重置为0
            probeCount = 0;
            rtt_noload = rtt;
            return (int)estimatedLimit;
        }
        //如果rtt小于rtt_noload，那么就不会更新estimatedLimit
        if (rtt_noload == 0 || rtt < rtt_noload) {
            rtt_noload = rtt;
            return (int)estimatedLimit;
        }

        //这里才进入limit更新的代码
        return updateEstimatedLimit(rtt, inflight, didDrop);
    }

    private int updateEstimatedLimit(long rtt, int inflight, boolean didDrop) {
        //先计算一个queuesize
        final int queueSize = (int) Math.ceil(estimatedLimit * (1 - (double)rtt_noload / rtt));

        double newLimit;
        // Treat any drop (i.e timeout) as needing to reduce the limit
        //如果是drop的，这里直接减小limit
        if (didDrop) {
            newLimit = decreaseFunc.apply(estimatedLimit);
            // Prevent upward drift if not close to the limit
        } else if (inflight * 2 < estimatedLimit) {
            //如果inflight没有靠近，那么其实也没有必要更新limit
            return (int)estimatedLimit;
        } else {
            int alpha = alphaFunc.apply((int)estimatedLimit);
            int beta = betaFunc.apply((int)estimatedLimit);
            int threshold = this.thresholdFunc.apply((int)estimatedLimit);

            // Aggressive increase when no queuing
            if (queueSize <= threshold) {
                //快速增长
                newLimit = estimatedLimit + beta;
                // Increase the limit if queue is still manageable
            } else if (queueSize < alpha) {
                //缓慢增长
                newLimit = increaseFunc.apply(estimatedLimit);
                // Detecting latency so decrease
            } else if (queueSize > beta) {
                //queuesize超过beta，则减小limit
                newLimit = decreaseFunc.apply(estimatedLimit);
                // We're within he sweet spot so nothing to do
            } else {
                return (int)estimatedLimit;
            }
        }
        newLimit = Math.max(1, Math.min(maxLimit, newLimit));
        //光滑处理，默认值是0，也就是控制newLimit中estimatedLimit和newLimit的占比
        newLimit = (1 - smoothing) * estimatedLimit + smoothing * newLimit;
        estimatedLimit = newLimit;
        return (int)estimatedLimit;
    }

    @Override
    public String toString() {
        return "VegasLimit [limit=" + getLimit() +
            ", rtt_noload=" + TimeUnit.NANOSECONDS.toMicros(rtt_noload) / 1000.0 +
            " ms]";
    }
}

