package org.apache.dubbo.rpc.limit;

import java.util.function.Consumer;

//limit algorithm interface
public interface Limit {
    //get current estimated limit
    int getLimit();

    //register a callback to receive notification whenever the limit is updated to a new value
    void notifyOnChange(Consumer<Integer> consumer);

    //update the limit value
    void update(long startTime, long rtt, int inflight, boolean didDrop);
}
