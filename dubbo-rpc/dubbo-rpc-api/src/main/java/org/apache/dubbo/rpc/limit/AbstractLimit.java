package org.apache.dubbo.rpc.limit;


import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public abstract class AbstractLimit implements Limit {
    //concurrency limit
    private volatile int limit;
    //用于更新limiter的limit值
    private final List<Consumer<Integer>> listeners = new CopyOnWriteArrayList<>();

    protected AbstractLimit(int initialLimit) {
        this.limit = initialLimit;
    }

    @Override
    public final synchronized void update(long startTime, long rtt, int inflight, boolean didDrop) {
        setLimit(_update(startTime, rtt, inflight, didDrop));
    }

    protected abstract int _update(long startTime, long rtt, int inflight, boolean didDrop);

    @Override
    public final int getLimit() {
        return limit;
    }

    protected synchronized void setLimit(int newLimit) {
        if (newLimit != limit) {
            limit = newLimit;
            listeners.forEach(listener -> listener.accept(newLimit));
        }
    }

    //注册listener的回调
    public void notifyOnChange(Consumer<Integer> consumer) {
        this.listeners.add(consumer);
    }


}

