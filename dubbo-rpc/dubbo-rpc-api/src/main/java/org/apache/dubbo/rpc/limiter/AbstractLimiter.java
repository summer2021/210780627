package org.apache.dubbo.rpc.limiter;

import org.apache.dubbo.rpc.limit.Limit;
import org.apache.dubbo.rpc.limit.VegasLimit;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public abstract class AbstractLimiter implements Limiter{
    private final Limit limitAlgorithm;
    private final AtomicInteger inflight = new AtomicInteger();
    private final Supplier<Long> clock = System::nanoTime;
    private volatile int limit;

    public static abstract class Builder {
        private Limit limitAlgorithm = VegasLimit.newBuilder().build();
        private Supplier<Long> clock = System::nanoTime;

        public Builder Limit(Limit limit){
            this.limitAlgorithm = limit;
            return this;
        }

        public abstract Builder self();
    }

    public AbstractLimiter(Builder builder){
        this.limitAlgorithm = builder.limitAlgorithm;
        this.limit = builder.limitAlgorithm.getLimit();
        this.limitAlgorithm.notifyOnChange(this::newLimit);
    }

    private void newLimit(int limit){
        this.limit= limit;
    }

    public int getLimit(){
        return this.limit;
    }

    public int getRemain(){
        return this.limit - this.inflight.get();
    }

    public int getInflight(){
        return this.inflight.get();
    }


    public Listener createListener(){
        final long startTime  = clock.get();
        final int currentInflight = inflight.incrementAndGet();
        return new Listener() {
            @Override
            public void onSuccess() {
                inflight.decrementAndGet();
                limitAlgorithm.update(startTime,clock.get()-startTime,currentInflight,false);
            }

            @Override
            public void onIgnore() {
                inflight.decrementAndGet();
            }

            @Override
            public void onDropped() {
                inflight.decrementAndGet();
                limitAlgorithm.update(startTime,clock.get()-startTime,currentInflight,true);
            }
        };
    }

    public Optional<Listener> createRejectedListener(){
        return Optional.empty();
    }
}
