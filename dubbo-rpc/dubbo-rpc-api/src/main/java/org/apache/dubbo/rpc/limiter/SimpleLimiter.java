package org.apache.dubbo.rpc.limiter;

import java.util.Optional;

public class SimpleLimiter extends AbstractLimiter{

    public static class Builder extends AbstractLimiter.Builder{
        @Override
        public AbstractLimiter.Builder self() {
            return this;
        }

        public static Builder newBuilder(){
            return new Builder();
        }
    }


    public static SimpleLimiter newDeafult(){
        return new SimpleLimiter(Builder.newBuilder());
    }

    public SimpleLimiter(Builder builder) {
        super(builder);
    }

    @Override
    public Optional<Listener> acquire() {
        int currInflight = getInflight();
        if (currInflight>=getLimit()){
            return createRejectedListener();
        }
        return Optional.of(createListener());
    }
}
