package org.apache.dubbo.rpc.limiter;

import java.util.Optional;

@FunctionalInterface
public interface Limiter {

    interface Listener {

        void onSuccess();

        void onIgnore();

        void onDropped();
    }

    Optional<Listener> acquire();
}
